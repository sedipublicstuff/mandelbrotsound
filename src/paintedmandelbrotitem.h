/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef PAINTEDMANDELBROTITEM_H
#define PAINTEDMANDELBROTITEM_H

#include <QQuickPaintedItem>
#include "mandelbrotiterator.h"
#include "note.h"
#include "noteplayer.h"


class PaintedMandelbrotItem : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(int iterations READ iterations WRITE setIterations NOTIFY iterationsChanged)

public:
    PaintedMandelbrotItem();

    enum class ColorListType {
        SIMPLE_5 = 0,
        RED_GREEN_16 = 1,
        RED_YELLOW_16 = 2,
        RED_YELLOW_50 = 3
    };

    enum class NoteListType {
        SINGLE_PENTATONIC = 0,
        MULTI_PENTATONIC = 1,
        MULTI_BLUES = 2,
        MULTI_MINOR_TRIADS = 3,
        MULTI_MAJOR_TRIADS = 4,
    };

    void paint(QPainter *painter) override ;
    inline Complex mandelPosition(int &screenX, int &screenY);
    inline Complex positionFromMandel(double &mandelX, double &mandelY);

    Q_INVOKABLE int noteNumber(qreal px, qreal py, bool paintPath = false, int pathIter = -1);
    Q_INVOKABLE void playNoteNumber(int noteNumber);
    Q_INVOKABLE void silenceMidi() {m_player->playNote(-1);}
    Q_INVOKABLE int iterations() const;
    Q_INVOKABLE void setIterations(int iterations);
    Q_INVOKABLE void clearTrace();
    Q_INVOKABLE void fillColorList(int typeNr);
    Q_INVOKABLE void fillColorList(ColorListType type);
    Q_INVOKABLE void fillNoteList(int typeNr);
    Q_INVOKABLE void fillNoteList(NoteListType type);
    Q_INVOKABLE void pitchWheel(int voice, int value) {m_player->pitchWheel(voice, value);}
    Complex centerOfShapeList();

    qreal screenHeight() const;
    void setScreenHeight(const qreal &screenHeight);
    qreal screenWidth() const;
    void setScreenWidth(const qreal &screenWidth);

signals:
    void iterationsChanged();

public slots:
    void recalculate();
    void playPoint(qreal x, qreal y);
    void changeStartX(bool up);
    void zoom(qreal px, qreal py, bool in);
    void repaint();
    void recalculateDisplayFactor();
    void changeInstrument(int voice, int instrument);
    void stopPlaying();


private:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    MandelbrotIterator* m_mandel = new MandelbrotIterator();
    NotePlayer* m_player = nullptr;

    QList<QColor> m_colorList;
    QList <Note> m_noteList;

    Complex m_topLeft;
    Complex m_bottomRight;
    Complex m_mandelSize;
    Complex m_startX;
    int m_iterations;
    qreal m_displayFactor;

    QList<Shape*> m_shapeList;
    Complex m_shapeCenter;
};

#endif // PAINTEDMANDELBROTITEM_H
