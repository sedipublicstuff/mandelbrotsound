/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

import QtQuick 2.13
import QtQuick.Window 2.13
import MandelbrotItems 1.0
import QtQuick.Controls 2.13


PaintedMandelbrotItem {
    id: mandelItem
    width: Screen.width
    height: Screen.height-100 // this is hackish. I want the window (yes, it's no window) maximized

    onWidthChanged: { console.debug("oulfpufTZ"); mandelItem.recalculate() }
    onHeightChanged:mandelItem.recalculate()
    MouseArea {
        id: mArea
        property int lastNote: -1
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        onPressed: {
            if (mouse.button === Qt.RightButton) {
                playNoteNumber(-1);
            } else {
                var note = noteNumber(mouse.x, mouse.y, trace.checked, 60)
                playNoteNumber(note)
                lastNote = note
            }
        }
        onPositionChanged: {
            if (pressed) {
                var note = noteNumber(mouse.x, mouse.y, false, 60)
                if (note !== lastNote) {
                    playNoteNumber(note)
                    lastNote = note
                }
            }
        }
        onWheel: {
            if (wheel.buttons === Qt.MiddleButton) {
                changeStartX(wheel.angleDelta.y > 0 ? false : true)
            } else {
                zoom(wheel.x, wheel.y, wheel.angleDelta.y < 0 ? false : true)
            }
        }
    }

    Dialog {
        id: infoDialog
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        ScrollView {
            width: parent.width
            height: parent.height
            TextArea {
                id: textArea
                wrapMode: "WordWrap" // no idea why this causes a binding loop (at least in Qt 5.13.2)
                text: "MandelbrotSound is an experimental program to explore the Mandelbrot set with acoustic implementations. \n What you can see (and hear) are the iteration processes of the clicked spots. By definition, those are stable within the Mandelbrot set. However, on their way they cut quite a rug, drawing the weirdest - and sometimes absolutely beautiful! - patterns onto their complex dance floor. \n Of course, to find an acoustic equivalence to that, touches the principle of the problem, raised by Frank Zappa: 'Writing about music is like dancing about architecture': \n\n This is an experimental, graphically supported improvisation with computer generated music, generated to dances within the perhaps most wondrous architecture of mathematics. \n\n I'd love to receive  your feedback, if you can also 'hear' these shapes as I do have the feeling that I can.\n\nCopyright 2019 by Sebastian Diel, source code released under GPL V3\n"
            }
        }
        standardButtons: Dialog.Ok
    }

    Row {
        id: settingsRow
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        spacing: 5
        width: childrenRect.width
        property real rowHeight: 40
        property real separatorHeight: 15
        Column {
            spacing: 2
            id: settingsTitleCol
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.topMargin: 2
            width:  childrenRect.width
            Label {
                text: qsTr("Iterations")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Repaint")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Inside the set:")
                height: settingsRow.separatorHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "lightgray"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Trace")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Outside the set:")
                height: settingsRow.separatorHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "lightgray"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Instrument")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Color scheme")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                id: spinText6
                text: qsTr("Midi scheme")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("Other/general:")
                height: settingsRow.separatorHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "lightgray"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }
            Label {
                text: qsTr("")
                height: settingsRow.rowHeight;
                verticalAlignment: "AlignVCenter"
                background: Rectangle { color: "white"; width: settingsRow.width }
                leftPadding: 3
            }


        }

        Column {
            id: settingsCol
            spacing: 2
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.topMargin: 2
            width: childrenRect.width
            SpinBox {
                from: 1
                to: 10000
                value: iterations
                onValueChanged: {
                    if (value !== iterations) {
                        setIterations(value)
                        if (alwaysRepaint.checked) {
                            repaint()
                        }
                    }
                }
                height: settingsRow.rowHeight;
                stepSize: value >= 500 ? 100 : (value < 50 ? (value < 10 ? 1: 5) : 50)
            }
            Row {
                height: settingsRow.rowHeight;
                CheckBox {
                    id: alwaysRepaint
                    checked: false
                    height: settingsRow.rowHeight;
                }
                Button {
                    text: "now"
                    onClicked: repaint()
                    height: settingsRow.rowHeight;
                }
            }
            Label {
                text: qsTr("")
                height: settingsRow.separatorHeight;
                verticalAlignment: "AlignVCenter"
                leftPadding: 3
            }
            CheckBox {
                id: trace
                checked: true
                height: settingsRow.rowHeight;
                onCheckedChanged: {
                    if (!checked) {
                        clearTrace();
                    }
                }
            }
            Label {
                text: qsTr("")
                height: settingsRow.separatorHeight;
                verticalAlignment: "AlignVCenter"
                leftPadding: 3
            }
            SpinBox {
                id: instrument
                value: 0
                from: 0
                to: 255
                onValueChanged: {
                    var ins = value
                    changeInstrument(0,ins)
                }
            }
            ComboBox {
                model: ["Simple (5)","Red to Green (16)","Red to Yellow (16)", "Red to Yellow (50)"]
                height: settingsRow.rowHeight
                currentIndex: 3
                onCurrentIndexChanged: fillColorList(currentIndex)
            }
            ComboBox {
                model: ["Single Pentatonic","Multi Pentatonic","Multi Blues", "Multi Minor Triads", "Multi Major Triads"]
                height: settingsRow.rowHeight
                currentIndex: 1
                onCurrentIndexChanged: fillNoteList(currentIndex)
            }
            Label {
                text: qsTr("")
                height: settingsRow.separatorHeight;
                verticalAlignment: "AlignVCenter"
                leftPadding: 3
            }
            Button {
                height: settingsRow.rowHeight
                text: "Stop Sound"
                onClicked: {
                    stopPlaying();
                }
            }
            Button {
                height: settingsRow.rowHeight
                text: "Info/About"
                onClicked: {
                    infoDialog.open()
                }
            }
        }
    }
}

