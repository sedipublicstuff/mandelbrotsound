/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "line.h"

Line::Line()
    :Shape()
{
    m_shapeType = Shape::ShapeType::LINE;
}

Line::Line(Complex &p1, Complex &p2)
{
    m_shapeType = Shape::ShapeType::LINE;
    m_p1 = p1;
    m_p2 = p2;
}

Complex Line::p1() const
{
    return m_p1;
}

void Line::setP1(const Complex &p1)
{
    m_p1 = p1;
}

Complex Line::p2() const
{
    return m_p2;
}

void Line::setP2(const Complex &p2)
{
    m_p2 = p2;
}

double Line::length() {
    double dX = qAbs(m_p2.x()-m_p1.x());
    double dY = qAbs(m_p2.iy()-m_p1.iy());
    return qSqrt(dX*dX+dY*dY);
}
