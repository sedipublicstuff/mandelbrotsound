/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "note.h"
#include <qmath.h>

Note::Note(int noteNumber)
{
    m_noteNumber = noteNumber;
}

int Note::noteNumber() const
{
    return m_noteNumber;
}

void Note::setNoteNumber(int noteNumber)
{
    m_noteNumber = noteNumber;
}

int Note::pitchWheel() const
{
    return m_pitchWheel;
}

void Note::setPitchWheel(int pitchWheel)
{
    m_pitchWheel = pitchWheel;
}

Note Note::fromFrequency(double frequency)
{
    if (frequency < 27.5) return Note(-1);

    // 12th root of 2 (-->equally tempered chromatic scale)
    const double pitchConst = 1.0594630943592952646;
    // We could think about implementing other scales, even reflecting the
    // MIDI scheme for the Mandelbrot outside

    double noteFrequency = 27.5; // "A0", 3 octaves below concert pitch (440Hz)
    int noteNumber = 21;
    while (noteFrequency < frequency) {
        noteFrequency *= pitchConst;
        noteNumber++;
    }
    Note n(noteNumber);

    // The note will probably not exactly match the desired frequency (probably to high)
    // We respect that in the pitchWheel property, making it possible to playback the
    // exact frequency. This is currently not being used here and it's not thoroughly
    // tested for sanity (though it seems to work), so beware.
    double halfStep = noteFrequency-noteFrequency/pitchConst;
    double difference = noteFrequency-frequency;
    double adjust = 0x1000 * (difference / halfStep);
    n.setPitchWheel(0x2000 - qRound(adjust));

    return n;
}
