/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "mandelbrotiterator.h"

void MandelbrotIterator::setInitialParameters(Complex startX, int maxIter)
{
    m_startX = startX;
    m_maxIter = maxIter;
}

// plain and simple, easy to understand. Not used, actually.
int MandelbrotIterator::mandelbrot(Complex &c, int maxIter)
{
    Complex x = m_startX;
    int i = 0;
    do {
        i++;
        x.square();
        x.add(c);
    } while (x.vectorSquaredLength() <= 4 && i <= maxIter);
    if (i > maxIter) {
        i = -1;
    }
    return i;

}

// checks for body (cycloid) and head (circle) of the set, is optimized by removing function calls from iteration
// the checks look expensive, actually they do no real harm in benchmarking. "It's the iterations, stupid" :-)
int MandelbrotIterator::mandelbrotPlusTest(Complex &c, int maxIter)
{
    double r = c.vectorSquaredLength();
    double s = qSqrt(r-0.5 * c.x() + 0.0625);
    if (!startX().isNull() || ((16 * r * s > 5 * s - 4 * c.x() + 1) && ((c.x()+1)*(c.x()+1))+(c.iy()*c.iy())>0.0625)) {
        double x_x = m_startX.x();
        double x_iy = m_startX.iy();
        double c_x = c.x();
        double c_iy = c.iy();
        int i = 0;
        double xx_square = x_x * x_x;
        double xiy_square = x_iy * x_iy;
        do {
            i++;
            double temp = xx_square - xiy_square + c_x;
            x_iy = 2 * x_x * x_iy + c_iy;
            x_x = temp;
            xx_square = x_x * x_x;
            xiy_square = x_iy * x_iy;
        } while (xx_square + xiy_square <= 4 && i <= maxIter);
        if (i > maxIter) {
            i = -1;
        }
        return i;
    }
    return -1;
}

// used for showing the iteration process inside the Mandelbrot set.
// Thus, we have no body or head checks, of course
// Also, the above optimizations are not as necessary as above, so - for better code
// readability - we just use what's there.
int MandelbrotIterator::mandelbrotPath(Complex &c, int maxIter, QList<Shape*> &list, Complex &finalX, int pathIterations)
{
    for (auto s : list) {
        if (s) s->deleteLater();
    }
    list.clear();
    Complex x = m_startX;
    int i = 0;
    do {
        i++;
        x.square();
        x.add(c);
        if (i <= pathIterations) {
            if (list.isEmpty()) {
                list.append(new Line(c,x));
            } else {
                auto lastCircle = static_cast<Circle*>(list.last());
                if (lastCircle) {
                    auto center = lastCircle->center();
                    list.append(new Line(center, x));
                } else Q_ASSERT(false);
            }
            list.append(new Circle(x,4));
        }
    } while (x.vectorSquaredLength() <= 4 && i <= maxIter);
    finalX = x;
    if (i > maxIter) {
        i = -1;
    }
    return i;
}

Complex MandelbrotIterator::startX() const
{
    return m_startX;
}

void MandelbrotIterator::setStartX(const Complex &startX)
{
    m_startX = startX;
}
