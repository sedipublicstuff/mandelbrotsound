/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef NOTEPLAYER_H
#define NOTEPLAYER_H

#include <QObject>
#include "3rdparty/QMidi/src/QMidiOut.h"
#include "circle.h"
#include "line.h"

class NotePlayer : public QObject
{
    Q_OBJECT
public:
    explicit NotePlayer(QObject *parent = nullptr);

signals:

public slots:
    void stop();
    void playNote(int noteNumber, int pitch = 0x2000);
    void setInstrument(int voice, int ins);
    void pitchWheel(int voice, int value) {m_midiOut->pitchWheel(voice, value);}
    void prepareForShapeList(QList<Shape*> &shapeList, Complex &lastX);
    void resetShapeList();
    void playShapeListNextStep();

private:
    QMidiOut* m_midiOut = nullptr;
    int m_playShapeIndex = -1;
    QList<Shape*> m_shapeList;
    Complex m_shapeCenter = Complex(0,0);
};

#endif // NOTEPLAYER_H
