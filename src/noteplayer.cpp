/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "noteplayer.h"
#include <QTimer>
#include <QDebug>
#include "note.h"


NotePlayer::NotePlayer(QObject *parent) : QObject(parent)
{
    m_midiOut = new QMidiOut();
    m_midiOut->connect("0");
}

void NotePlayer::stop() {
    m_midiOut->noteOn(-1, 0, 64);
}

void NotePlayer::playNote(int noteNumber, int pitch)
{
    if (noteNumber > 127){
        return;
    }
    if (pitch != 0x2000) {
        m_midiOut->noteOn(-1, 0, 64);
        m_midiOut->pitchWheel(0, pitch);
    }
    m_midiOut->noteOn(noteNumber, 0, 64);
}

void NotePlayer::setInstrument(int voice, int ins)
{
    m_midiOut->setInstrument(voice, ins);
}

void NotePlayer::playShapeListNextStep()
{
    m_playShapeIndex++;
    if (!m_shapeList.isEmpty() && m_shapeList.count() > m_playShapeIndex) {
        auto s = m_shapeList.value(m_playShapeIndex);
        if (s && s->shapeType() == Shape::ShapeType::LINE) {
            auto line = static_cast<Line*>(s);
            auto length = line->length();
            int duration = qRound(500*length);
            auto distance = line->p1().distanceTo(m_shapeCenter);
            auto frequency = 100/distance;
            auto n = Note::fromFrequency(frequency);
            QTimer::singleShot(duration,this, SLOT(playShapeListNextStep()));
            // Playing the un"rounded" pitch sounds crappy. We use chromatic scale here.
//            playNote(n.noteNumber(), n.pitchWheel());
            playNote(n.noteNumber());
        } else {
            // jump over circles (or other shapes we don't know yet)
            if (s) {
                playShapeListNextStep();
            } else {
                resetShapeList();
            }
        }
    } else {
        resetShapeList();
    }
}

void NotePlayer::prepareForShapeList(QList<Shape*> &shapeList, Complex &shapeCenter)
{
    m_playShapeIndex = -1;
    m_shapeList.clear();
    for (auto s : shapeList) {
        m_shapeList.append(s);
    }
    m_shapeCenter = shapeCenter;
}

void NotePlayer::resetShapeList() {
    m_shapeList.clear();
    m_playShapeIndex = -1;
    m_shapeCenter = Complex(0,0);
}
