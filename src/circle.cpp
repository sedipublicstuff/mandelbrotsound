/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "circle.h"

Circle::Circle()
    :Shape()
{
    m_shapeType = Shape::ShapeType::CIRCLE;
}

Circle::Circle(Complex &c, double radius)
    :Shape()
{
    m_shapeType = Shape::ShapeType::CIRCLE;
    m_center = c;
    m_pixelRadius = radius;
}

Complex Circle::center() const
{
    return m_center;
}

void Circle::setCenter(const Complex &center)
{
    m_center = center;
}

double Circle::pixelRadius() const
{
    return m_pixelRadius;
}

void Circle::setPixelRadius(double pixelRadius)
{
    m_pixelRadius = pixelRadius;
}

double Circle::distanceTo(Complex &other)
{
    double dX = qAbs(m_center.x()-other.x());
    double dY = qAbs(m_center.iy()-other.iy());
    return qSqrt(dX*dX+dY*dY);
}
