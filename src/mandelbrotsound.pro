#
# Copyright (C) 2019 Sebastian Diel
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/> */
#

QT += quick
#QMAKE_CXXFLAGS += -o3
#QMAKE_CXXFLAGS += -Ofast


CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        circle.cpp \
        complex.cpp \
        line.cpp \
        main.cpp \
        mandelbrotiterator.cpp \
        note.cpp \
        noteplayer.cpp \
        paintedmandelbrotitem.cpp \
        shape.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    circle.h \
    complex.h \
    line.h \
    mandelbrotiterator.h \
    note.h \
    noteplayer.h \
    paintedmandelbrotitem.h \
    shape.h

include(3rdparty/QMidi/src/QMidi.pri)

DISTFILES += \
    3rdparty/QMidi/src/QMidi.pri
