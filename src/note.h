/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef NOTE_H
#define NOTE_H


class Note
{
public:
    Note(int noteNumber = 60);

    int noteNumber() const;
    void setNoteNumber(int noteNumber);

    int pitchWheel() const;
    void setPitchWheel(int pitchWheel);

    static Note fromFrequency(double frequency);

private:
    int m_noteNumber;
    int m_pitchWheel = 0x2000;

};

#endif // NOTE_H
