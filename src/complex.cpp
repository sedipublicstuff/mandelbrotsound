/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "complex.h"

Complex::Complex()
{
    m_x = 0;
    m_iy = 0;
}

Complex::Complex(const Complex &c)
{
    m_x = c.x();
    m_iy = c.iy();
}

double Complex::x() const
{
    return m_x;
}

void Complex::setX(double &x)
{
    m_x = x;
}

double Complex::iy() const
{
    return m_iy;
}

void Complex::setIY(double &y)
{
    m_iy = y;
}

Complex Complex::multiply(Complex &c1, Complex &c2)
{
    double x = (c1.x() * c2.x()) - (c1.iy() * c2.iy());
    double iy = (c1.x() * c2.iy()) + (c1.iy() * c2.x());
    return Complex(x,iy);
}

double Complex::distanceTo(Complex &other)
{
    double dX = qAbs(m_x-other.x());
    double dY = qAbs(m_iy-other.iy());
    return qSqrt(dX*dX+dY*dY);
}

bool Complex::isNull()
{
    return qFuzzyIsNull(m_x) && qFuzzyIsNull(m_iy);
}

Complex &Complex::operator=(Complex &other) {
    m_x = other.x();
    m_iy = other.iy();
    return *this;
}
