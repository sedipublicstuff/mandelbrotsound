/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#include "paintedmandelbrotitem.h"
#include <QPainter>
#include <QtMath>
#include "mandelbrotiterator.h"
#include "noteplayer.h"
#include <QTimer>
#include <QElapsedTimer>

PaintedMandelbrotItem::PaintedMandelbrotItem()
{

    // Starting points, useful for benchmarking etc.

    //standard
    m_topLeft = Complex(-2, 1.1);
    m_mandelSize = Complex(2.5, 2.2);
    m_iterations = 1000;

    // side/mix
//    m_topLeft = Complex(-0.755771, 0.111633);
//    m_mandelSize = Complex(0.00976563, 0.00859375);
//    m_iterations = 1000;

    //outside
//    m_topLeft = Complex(-0.749763, 0.107951);
//    m_mandelSize = Complex(0.0012207, 0.00107422);
//    m_iterations = 1000;

    m_startX = Complex(0,0);
    m_mandel->setInitialParameters(m_startX, 0);
    m_displayFactor = qMax(m_mandelSize.x()/width(), m_mandelSize.iy()/height());
    m_player = new NotePlayer();

    setAcceptedMouseButtons(Qt::AllButtons);
    setRenderTarget(QQuickPaintedItem::Image);

    fillColorList(ColorListType::RED_YELLOW_50);
    fillNoteList(NoteListType::MULTI_PENTATONIC);
}

void PaintedMandelbrotItem::fillColorList(int typeNr)
{
    fillColorList(static_cast<ColorListType>(typeNr));
}

void PaintedMandelbrotItem::fillColorList(PaintedMandelbrotItem::ColorListType type)
{
    m_colorList.clear();
    switch (type) {
        case ColorListType::SIMPLE_5:
            m_colorList << QColor(Qt::red) << QColor(Qt::green) << QColor(Qt::yellow) << QColor(Qt::blue);
            break;
        case ColorListType::RED_GREEN_16:
            m_colorList << QColor("#ff0000") << QColor("#EE1100") << QColor("#dd2200") << QColor("#cc3300")
                        << QColor("#bb4400") << QColor("#aa5500") << QColor("#996600") << QColor("#887700")
                        << QColor("#778800") << QColor("#669900") << QColor("#55aa00") << QColor("#44bb00")
                        << QColor("#33cc00") << QColor("#22dd00") << QColor("#11ee00") << QColor("#00ff00");
            break;
        case ColorListType::RED_YELLOW_16:
            m_colorList << QColor("#ff0000") << QColor("#ff1100") << QColor("#ff2200") << QColor("#ff3300")
                        << QColor("#ff4400") << QColor("#ff5500") << QColor("#ff6600") << QColor("#ff7700")
                        << QColor("#ff8800") << QColor("#ff9900") << QColor("#ffaa00") << QColor("#ffbb00")
                        << QColor("#ffcc00") << QColor("#ffdd00") << QColor("#ffee00") << QColor("#ffff00");
            break;
        case ColorListType::RED_YELLOW_50:
            for (int i = 0; i < 50; i++) {
                m_colorList << QColor(255, 5*i, 0);
            }
    }
    repaint();
}

void PaintedMandelbrotItem::fillNoteList(int typeNr)
{
    fillNoteList(static_cast<NoteListType>(typeNr));
}

void PaintedMandelbrotItem::fillNoteList(PaintedMandelbrotItem::NoteListType type)
{
    int startNote = -1;
    m_noteList.clear();
    switch (type) {
    case NoteListType::SINGLE_PENTATONIC:
        m_noteList << Note(60) << Note(62) << Note(65) << Note(67) <<  Note(69);
        break;
    case NoteListType::MULTI_PENTATONIC:
        startNote = 20;
        for (int i = 0; i<9 ; i++) {
            m_noteList << Note(startNote + 12*i + 0);
            m_noteList << Note(startNote + 12*i + 2);
            m_noteList << Note(startNote + 12*i + 5);
            m_noteList << Note(startNote + 12*i + 7);
            m_noteList << Note(startNote + 12*i + 9);
        }
        break;
    case NoteListType::MULTI_BLUES:
        startNote = 20;
        for (int i = 0; i<9 ; i++) {
            m_noteList << Note(startNote + 12*i + 0);
            m_noteList << Note(startNote + 12*i + 3);
            m_noteList << Note(startNote + 12*i + 5);
            m_noteList << Note(startNote + 12*i + 6);
            m_noteList << Note(startNote + 12*i + 7);
            m_noteList << Note(startNote + 12*i + 9);
        }
        break;
    case NoteListType::MULTI_MINOR_TRIADS:
        startNote = 20;
        for (int i = 0; i<9*4 ; i++) {
            m_noteList << Note(startNote + 3*i);
        }
        break;
    case NoteListType::MULTI_MAJOR_TRIADS:
        int startNote = 20;
        for (int i = 0; i<9*3 ; i++) {
            m_noteList << Note(startNote + 4*i);
        }
        break;
    }
}

Complex PaintedMandelbrotItem::centerOfShapeList()
{
    double sumX = 0;
    double sumY = 0;
    int lineCount = 0;
    for (auto s : m_shapeList) {
        if (s->shapeType() == Shape::ShapeType::LINE) {
            auto l = static_cast<Line*>(s);
            if (l) {
                sumX += l->p1().x();
                sumY += l->p1().iy();
                lineCount++;
            }
        }
    }
    if (lineCount > 0) {
        return Complex(sumX/lineCount, sumY/lineCount);
    } else {
        return Complex(0,0);
    }
}

void PaintedMandelbrotItem::paint(QPainter *painter)
{
#ifdef QT_DEBUG
    QElapsedTimer t;
    t.start();
#endif

    // "rectSize" is a preparation for the possibility to not calculate every pixel but force
    // a "quick and dirty" mode with huge pixels.
    // not currently in use, as "drawRect" is commented out below.
    const int rectSize = 1;

    for (int xval = 0; xval < width() ; xval+=rectSize ) {
        for (int yval = 0; yval < height() ; yval+=rectSize ) {
            auto c = this->mandelPosition(xval, yval);
            int colNr = m_mandel->mandelbrotPlusTest(c, m_iterations);
            if (colNr < 0) {
                painter->setPen(QColor(Qt::black));
                painter->setBrush(QBrush(QColor(Qt::black)));
            } else {
                painter->setPen(m_colorList.value(colNr % m_colorList.count()));
                painter->setBrush(QBrush(m_colorList.value(colNr % m_colorList.count())));
            }
            painter->drawPoint(xval, yval);
//            painter->drawRect(xval, yval, rectSize, rectSize);
        }        
    }
#ifdef QT_DEBUG
    qDebug()<<"Paint needed "<< t.elapsed() <<"ms";
    qDebug()<<"x:"<<m_topLeft.x()<<" / y:"<<m_topLeft.iy()<<"  w:"<<m_mandelSize.x()<<" / h:"<<m_mandelSize.iy()<<" ("<<m_iterations<<" iterations)";
#endif
    if (!m_shapeList.isEmpty()) {
        painter->setPen(QColor(Qt::white));
        painter->setBrush(QBrush(QColor(Qt::white)));
        for (auto s : m_shapeList) {
            if (s->shapeType() == Shape::ShapeType::CIRCLE) {

                auto c = static_cast<Circle*>(s);
                auto mandelCenterX = c->center().x();
                auto mandelCenterY = c->center().iy();
                auto center = this->positionFromMandel(mandelCenterX, mandelCenterY);
                painter->drawEllipse(QPointF(center.x(), center.iy()),c->pixelRadius()/2, c->pixelRadius()/2);

            } else if (s->shapeType() == Shape::ShapeType::LINE) {

                auto l = static_cast<Line*>(s);
                auto p1X = l->p1().x();
                auto p1Y = l->p1().iy();
                auto mandelP1 = this->positionFromMandel(p1X, p1Y);
                auto p2X = l->p2().x();
                auto p2Y = l->p2().iy();
                auto mandelP2 = this->positionFromMandel(p2X, p2Y);
                painter->drawLine(qRound(mandelP1.x()), qRound(mandelP1.iy()), qRound(mandelP2.x()), qRound(mandelP2.iy()));
            }
        }
        auto shapeCenterX = m_shapeCenter.x();
        auto shapeCenterY = m_shapeCenter.iy();
        auto shapeCenter = positionFromMandel(shapeCenterX, shapeCenterY);
        painter->setPen(QColor(Qt::blue));
        painter->setBrush(QBrush(QColor(Qt::blue)));
        painter->drawEllipse(qRound(shapeCenter.x()), qRound(shapeCenter.iy()),5,5);
    }
}

Complex PaintedMandelbrotItem::mandelPosition(int &screenX, int &screenY)
{
    double mx = (screenX * m_displayFactor) + m_topLeft.x();
    double miy = -(screenY * m_displayFactor) + m_topLeft.iy();
    return Complex (mx, miy);
}

Complex PaintedMandelbrotItem::positionFromMandel(double &mandelX, double &mandelY) {
    double scrX = (mandelX - m_topLeft.x()) / m_displayFactor;
    double scrY = -(mandelY - m_topLeft.iy()) / m_displayFactor;
    return Complex (scrX, scrY);
}

int PaintedMandelbrotItem::noteNumber(qreal px, qreal py, bool paintPath, int pathIter)
{
    if (pathIter < 0) pathIter = m_iterations;
    int pxInt = qRound(px);
    int pyInt = qRound(py);
    auto c = mandelPosition(pxInt, pyInt);
    int result = -1;
    if (paintPath) {

        // We need a reference point for calculating the sound frequency, prefereably the center of the series.
        // finalX is the endpoint, where iterations give up. That's a good choice,
        // when the series is converging, a bad one when it's alternating.
        // that's why we better properly calculate the shapeList's center instead.
        // Thus, finalX remains unused for now.
        Complex finalX;

        result = m_mandel->mandelbrotPath(c, m_iterations, m_shapeList, finalX, pathIter);
        if (result >=0 ) {
            clearTrace();
        }
//        m_player->playShapeList(m_shapeList, finalX); // see above
        auto shapesCenter = this->centerOfShapeList();
        m_shapeCenter = shapesCenter;
        if (result < 0) {
            m_player->prepareForShapeList(m_shapeList, shapesCenter);
            QTimer::singleShot(100, m_player, SLOT(playShapeListNextStep())); //wait (for better rhythmical correctness)
        }
        repaint();

    } else {
        result = m_mandel->mandelbrot(c, m_iterations);
    }
    if (result >= 0) {
        return m_noteList.value(result % m_noteList.count()).noteNumber();
    } else {
        return -1;
    }
}

void PaintedMandelbrotItem::playNoteNumber(int noteNumber)
{
    m_player->playNote(noteNumber);
}

void PaintedMandelbrotItem::recalculate()
{
    m_displayFactor = qMax(m_mandelSize.x()/width(), m_mandelSize.iy()/height());
}

void PaintedMandelbrotItem::playPoint(qreal px, qreal py)
{
    int pxInt = qRound(px);
    int pyInt = qRound(py);
    auto c = mandelPosition(pxInt, pyInt);
    auto result = m_mandel->mandelbrot(c, 200);
    m_player->playNote(m_noteList.value(result % m_noteList.count()).noteNumber());
}

void PaintedMandelbrotItem::changeStartX(bool up) {
    double delta = 0.1 * (up ? 1 : -1);
    m_startX = Complex(m_startX.x()+delta, m_startX.iy()+delta);
    m_mandel->setStartX(m_startX);
    this->update(QRect(0,0,qRound(width()), qRound(height())));
}

void PaintedMandelbrotItem::zoom(qreal px, qreal py, bool in)
{
    int pxInt = qRound(px);
    int pyInt = qRound(py);
    auto c = mandelPosition(pxInt, pyInt);

    double mandelWidth = in ? m_mandelSize.x() / 2 : m_mandelSize.x() * 2;
    double mandelHeight = in ? m_mandelSize.iy() / 2 : m_mandelSize.iy() * 2;
    m_mandelSize = Complex (mandelWidth, mandelHeight);

    double left = c.x() - mandelWidth/2;
    double top = c.iy() + mandelHeight/2;
    m_topLeft = Complex (left, top);

    this->geometryChanged(QRectF(), QRectF());
    repaint();
}

void PaintedMandelbrotItem::repaint()
{
    this->update(QRect(0,0,qRound(width()), qRound(height())));
}

void PaintedMandelbrotItem::recalculateDisplayFactor()
{
    m_displayFactor = qMax(m_mandelSize.x()/width(), m_mandelSize.iy()/height());
}

void PaintedMandelbrotItem::changeInstrument(int voice, int instrument)
{
    m_player->setInstrument(voice, instrument);
}

void PaintedMandelbrotItem::stopPlaying()
{
    m_player->resetShapeList();
    m_player->playNote(-1);
}

void PaintedMandelbrotItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickPaintedItem::geometryChanged(newGeometry, oldGeometry);
    recalculateDisplayFactor();
}

int PaintedMandelbrotItem::iterations() const
{
    return m_iterations;
}

void PaintedMandelbrotItem::setIterations(int iterations)
{
    m_iterations = iterations;
}

void PaintedMandelbrotItem::clearTrace()
{
    for (auto s: m_shapeList) {
        s->deleteLater();
    }
    m_shapeList.clear();
    repaint();
}
