/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef LINE_H
#define LINE_H
#include "shape.h"

class Line : public Shape
{
public:
    Line();
    Line(Complex &p1, Complex &p2);

    Complex p1() const;
    void setP1(const Complex &p1);

    Complex p2() const;
    void setP2(const Complex &p2);

    double length();

protected:
    Complex m_p1 = Complex(0,0);
    Complex m_p2 = Complex(0,0);
};

#endif // LINE_H
