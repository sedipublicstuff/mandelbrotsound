/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.h"


class Circle : public Shape
{
public:
    Circle();
    Circle(Complex &c, double radius = 10);

    Complex center() const;
    void setCenter(const Complex &center);

    double pixelRadius() const;
    void setPixelRadius(double pixelRadius);

    double distanceTo(Complex &other);

protected:
    Complex m_center = Complex(0,0);
    double m_pixelRadius = 10;

};

#endif // CIRCLE_H
