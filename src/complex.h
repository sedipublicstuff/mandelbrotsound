/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef COMPLEX_H
#define COMPLEX_H

#include <QtMath>

class Complex
{
public:
    Complex();
    Complex(const Complex &c);
    Complex(double x, double iy) { m_x = x; m_iy = iy;}

    Complex& operator = (const Complex &c)
    {
        m_x = c.x();
        m_iy = c.iy();
        return *this;
    }

    double x() const;
    void setX(double &x);

    double iy() const;
    void setIY(double &iy);

    inline double vectorSquaredLength() {return m_x * m_x + m_iy * m_iy; }
    inline double vectorLength() {return qSqrt((m_x * m_x) + (m_iy * m_iy)); }
    inline Complex squared() {
        double x = (m_x * m_x) - (m_iy * m_iy);
        double iy = 2 * (m_x * m_iy);
        return Complex(x,iy);
    }
    inline void square() {
        double x = m_x * m_x - m_iy * m_iy;
        m_iy = 2 * m_x * m_iy;
        m_x = x;
    }

    inline void squareAndAdd(Complex &other) {
        double x = m_x * m_x - m_iy * m_iy + other.x();
        m_iy = 2 * m_x * m_iy + other.iy();
        m_x = x;
    }

    static inline Complex multiply(Complex &c1, Complex &c2);

    inline void add (Complex other) {
        m_x += other.x();
        m_iy += other.iy();
    }
    double distanceTo(Complex &other);

    Complex& operator=(Complex& other);
    bool isNull();

protected:
    double m_x;
    double m_iy;
};


#endif // COMPLEX_H
