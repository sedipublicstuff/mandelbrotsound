/*
 * Copyright (C) 2019 Sebastian Diel
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>
 */

#ifndef MANDELBROTITERATOR_H
#define MANDELBROTITERATOR_H

#include "complex.h"
#include "circle.h"
#include "line.h"

class MandelbrotIterator
{
public:
    MandelbrotIterator() {}
    ~MandelbrotIterator() {}

    void init() {}
    void setInitialParameters(Complex startX, int maxIter);
    int mandelbrot(Complex &c, int maxIter);
    int mandelbrotPlusTest(Complex &c, int maxIter);
    int mandelbrotPath(Complex &c, int maxIter, QList<Shape *> &list, Complex &finalX, int pathIterations);

    Complex startX() const;
    void setStartX(const Complex &startX);

private:
    Complex m_startX;
    int m_maxIter;
};

#endif // MANDELBROTITERATOR_H
